import React from "react";
import VerticleGalleryImages from "./VerticleGalleryImages";

export default function ProductVerticleSlider({ product }) {
  return (
    <div>
      {product.gallery_images && (
        <VerticleGalleryImages
          swatch={product.swatch}
          gallery_images={product.gallery_images}
          height={800}
          width={800}
        />
      )}
    </div>
  );
}
